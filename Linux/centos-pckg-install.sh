#!/bin/sh
yum update -y
yum install epel-release -y
yum install -y \
https://repo.ius.io/ius-release-el7.rpm \
https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
yum install net-tools wget nmap telnet git ansible openssh-server curl zsh -y
