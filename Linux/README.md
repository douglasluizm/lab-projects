**OS Version**

`$ lsb-release-a`

`$ cat /proc/version`

`$ cat etc/release (?)`

**Network**

`Ifconfig`

**NTP settings**

`$ cat etc\rsyslog.conf`

**Local user settings**

`$ /etc/shadow`

`$ /etc/passwd`

**installed pckgs with version**

`$ rpm -qa`

`$ yum list installed`

`$ yum list kernel`

`$ yum history list`

**Listening ports**

`$ service --status-all`

**Process**

`$ top`
(ctrl + m to go to memory)
