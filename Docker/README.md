# Docker
## Installing Docker on Windows Subsystem for Linux (WSL) 1

To run Docker over WSL, you'll need to set up Docker for Windows first and enable non-encrypt config. You will also need to set up a system-variable


**update the system**
```sh
$ sudo apt-get update
```
**install packages to allow apt to use repository over https**
```sh
$ sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
```
**add key**
```sh
$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
**update the system again**
```sh
$ sudo apt-get update
```
**install docker and necessary packages**
```sh
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Selecting which storage driver to choose
Full documentation available on [docs.docker.com](https://docs.docker.com/storage/storagedriver/select-storage-driver/)

**check full docker info**
```sh
$ docker info
```
There are different ways you can set the storage driver type.

**1 - Explicitly providing a flag to the docker daemon**
```sh
## edit the ExecStartline adding --storage-driver devicemapper flag
$ sudo vi /usr/lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd --storage-driver devicemapper ...
```
**1.1 Reload systemd and restart docker**
```sh
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```
**2 - Using the daemon config file**
we can also set the storage driver explicitly using the daemon configuration file. This is the method that Docker recommends. Note that we cannot do this and pass the --storage-driver flag to the daemon at the same time:
ps: if file doesn't exists, create one in same location.
```sh
$ sudo vi /etc/docker/daemon.json
```
**2.1 add the following line (json format) in the daemon config file:**

```sh
{
  "storage-driver": "devicemapper"
}
```
**2.2 restart docker. You can also check docker status**
```sh
$ sudo systemctl restart docker
$ sudo systemctl status docker
```

### Container running

running a simple hello-world app to test docker

```sh
$ sudo docker run hello-world
```

if you want to set up the image tag (using nginx as example)

```sh
$ sudo docker run nginx:1.15.11
```

run a container with a command and arguments:

```sh
$ sudo docker run busybox echo hello world!
```

run an Nginx container customized with a variety of flags:

```sh
$ sudo docker run -d --name nginx --restart unless-stopped -p 8080:80 --memory 500M --memory-reservation 256M nginx
```

list any currently running containers:

```sh
$ sudo docker ps
```

list all containers, both running and stopped:

```sh
$ sudo docker ps -a
```

stop the Nginx container:

```sh
$ sudo docker container stop nginx
```

start a stopped container:

```sh
$ sudo docker container start nginx
```

delete a container (but it must be stopped first):

```sh
$ sudo docker container rm nginx
```
