verify if you have Portainer image:
```sh
$ docker images
```

If you don't, search for it:
```sh
$ docker search portainer
```

one of the first options is the portainer/portainer, the docker manage that we'll use. The others are agent, template or related stuff. Lets pull the portainer image:
```sh
$ docker pull portainer/portainer
```

check the docker images again to check if the portainer image is now available to you. In my case, I am using WSL1 (debian) and the Portainer will manage this host itself, but Portainer can also manage remote clusters.

create a volume for the portainer data:
```sh
$ docker volume create portainer_data
```

create the container using the image we downloaded before:
```sh
$ docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

##### Reference of each parameter:
```sh
"-d" = deatched mode
"-p" = port we will use to point our localhost to the portainer port
"--name" = container name
"--restart=always" = if the container exits, docker will restart it
"-v /var/run/docker.sock:/var/run/docker.sock" = for linux env
"portainer_data:/data portainer/portainer" = bind the mount volume
 ```

 - *Check the container running with docker ps. If its running correctly, open chrome or other browser and go to localhost:9000* 
 - *It will ask you for a new admin password. Set it. Select Local in the next page and click on Connect. You now have the*

